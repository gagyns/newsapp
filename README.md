# NEWS Aplication
 NewsApp is created with JS vanilla with minimal usage of Jquery in most cases as DOM element creator. 
 The app is partly responsive(in terms of supported resolutions). While app development for mobile resolution of iPhone 6/7/8 was a reference.
 The development of the app was tried to use as much as possible only basic JS and Jquery and SASS for styling.
 Routing, State management, observables, and i18n tried to be done without any external library in goal in mind to show as much of possible 
 JS and JS design patterns skills.

Tehnologies used:
ES6 JS,
JQUERY,
SASS,
Webpack.

Webpack:
 - dev, prod mode,
 - ES6 Support with Babel,
 - JavaScript Linting with eslint-loader
 - SASS Support with sass-loader
 - Style Linting with stylelint


### Installation

```
npm install
```

### Start Dev Server

```
npm start
```

### Build Prod Version

```
npm run build
```

When you run `npm run build` we use the [mini-css-extract-plugin](https://github.com/webpack-contrib/mini-css-extract-plugin) to move the css to a separate file. The css file gets included in the head of the `index.html`.
