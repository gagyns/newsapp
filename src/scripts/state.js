import { globals } from './globals';
import { TopNewsPage } from './components/pages/topNewsPage';
import { FocusOnNewsPage } from './components/pages/focusOnNewsPage';
import { CategoriesPage } from './components/pages/categoriesPage';

/**
 * @class State
 * @method constructor - sets init STATE for app as next ste we can consider to fill initial state from external object
 * @method updateState receive stateEntity and value and make changes dynamical, with that in mind we dont need separate reducer
 * switch to decide witch state argument have to be updated. As method for updating use spread operator instead of Object.assign
 * also after update of state we call @callMethodsOnUpdate method and run all methods witch are assigned to as subscribes to be refreshed.
 * @getStateByEntity as method we get the State state by giving argument witch value we want to get.
 * @updateStateWithoutRender as @updateState same functionality but we dont call subscribed methods to update
 * @assignMethodForUpdate @param {method} here we put methods in array to be iterated and called on update
 * @callMethodsOnUpdate iterate and call subscribed methods
 */
export class State {
  constructor() {
    this.state = {
      topNewsData: [],
      filteredData: [],
      filterRegex: '',
      newsByCategory: new Map(),
      pagesTypes: [globals.PAGE_TOP_NEWS, globals.PAGE_CATEGORIES, globals.PAGE_SEARCH],
      activePage: globals.PAGE_TOP_NEWS,
      regionAvailable: [ globals.REGION_UNITED_STATES, globals.REGION_GREAT_BRITAIN ],
      newsRegion: globals.REGION_GREAT_BRITAIN,
      regionChooserActive: true,
      layout: globals.LAYOUT_DESKTOP,
      appLanguage: globals.LANGUAGE_ENGLISH,
      focusedCategory: '',
      newsForCategory: [],
      focusedArticle: {}
    };
    this.callOnUpdate = [];
  }
  updateState(stateEntity, value) {
    this.state = { ... this.state, [`${stateEntity}`]: value};
    this.callMethodsOnUpdate();
  }
  getStateByEntity(entity) {
    return this.state[entity];
  }
  updateStateWithoutRender(stateEntity, value) {
    this.state = { ... this.state, [`${stateEntity}`]: value};
  }
  assignMethodForUpdate(method) {
    this.callOnUpdate.push(method);
  }
  callMethodsOnUpdate() {
    if (this.callOnUpdate.length > 0) {
      this.callOnUpdate.forEach( method => method());
    }
  }
}
