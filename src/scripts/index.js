import '../styles/index.scss';
import { globals } from './globals';
import * as $ from 'jquery';
import { HeaderComponent } from './components/headerComponent';
import { MainContentComponent } from './components/mainContentComponent';
import { I18n } from './i18n/i18n';
import { createHtmlElement } from './helperFunstions';
import { TopNewsDataModel } from './dataModels/topNewsDataModel';
import { Router } from './router';

/**
 * Initialises MainApp with extending two classes Router and State
 * @namespace window.instanceMainApp
 * @class MainApp
 * @method constructor - sets id and data into class instance and also initialize Router and State class
 * @run Draw basic layout and fire ajax requests
 * @drawBasicLayout draw basic containers for main app layout in purpose to stay unchanger on page reinit
 * @getData call top news api and have to api variations in terms of region simulate 2 sec response time
 * @getDataForCategories call categories api and prepare data for categories page simulate 4 sec respone time
 */

class MainApp extends Router {
  constructor() {
    super();
    /**
     * Make instances of main components and send "this", we chose to send this throw the constructor
     * because bind, call, apply is not useful in this class instancing scenario.
     * @type { HeaderComponent }
     * @type { mainContentComponent }
     */
    this.headerComponent = new HeaderComponent(this);
    this.mainContentComponent = new MainContentComponent(this);
    this.appContainer = $('#app');
    this.labels = new I18n(this.getStateByEntity(globals.LANGUAGE_APP_LANGUAGE));
  }
  run() {
    this.drawBasicLayout();
    this.getData();
    this.getDataForCategories();
  }
  drawBasicLayout() {
    this.appContainer.empty();
    /**
     * In app we set device by resolution. 400 and more treated as Desktop and smaller treated as mobile device
     * at the end of dev this flag is only used to deside title string trim.
     */
    this.updateStateWithoutRender(globals.LAYOUT, window.innerWidth <= 400 ?
      globals.LAYOUT_MOBILE : globals.LAYOUT_DESKTOP);
    // Creating basic layout by using helper method witch is created as Factory pattern
    this.$mainContainer = createHtmlElement('div',
      `js-main-container big-main-container-props u_position-relative`);

    this.$sideBarContainer = createHtmlElement('div',
      'u_position-absolute big-screen-hide u_heigh-100vh u_min-width-200px ' +
      'u_postition-top-left-null u_background-white o_side-bar-menu o_side-bar-menu--inactive'
    );

    this.$headerContainer = createHtmlElement('div', 'u_width-100pct u_border-bottom-darkgray');
    this.$mainContainer.append(this.$headerContainer);
    this.headerComponent.drawHeader();

    this.$mainContentContainer = createHtmlElement(
      'div',
      `u_width-100pct main-conteiner-height-media`);

    this.$mainContainer.append(this.$mainContentContainer);
    this.$mainContainer.append(this.$sideBarContainer);

    this.mainContentComponent.drawMainContentLoader(this.$mainContainer);

    this.appContainer.append(this.$mainContainer);
  }
  getData() {
    this.mainContentComponent.drawMainContentLoader();
    this.activePageInStore = this.getStateByEntity(globals.PAGE_ACTIVE_PAGE);
    const regionChoosen = this.getStateByEntity(globals.NEWS_REGION).toLowerCase();
    $.ajax({
      context: this,
      type: 'GET',
      url: `https://newsapi.org/v2/top-headlines?country=${regionChoosen}&apiKey=846757f543c743a19ecef01a093abda9`,
      success: data => {
        if (data.articles.length) {
          // Response From Api is deserialize by TopNewsDataModel class in purpose to have JS object
          const rawData = data.articles.map((data, index) => new TopNewsDataModel()
            .deserialize(data, index));
          // Fill two Store fields because main data have to be unmute
          this.updateStateWithoutRender(globals.TOP_NEWS_DATA, rawData);
          this.updateStateWithoutRender(globals.FILTERED_DATA, rawData);
          // simulate API waiting 2 sec
          setTimeout(() => {
            this.routeChange(this.activePageInStore);
          }, 2000);
        }
      }
    });
  }
  getDataForCategories() {
    $.ajax({
      context: this,
      type:'GET',
      url: 'https://newsapi.org/v2/sources?apiKey=846757f543c743a19ecef01a093abda9',
      success: data => {
        // Response for api is sorted in map with key is category and value is array of news for that category
        // in that in mind we dont need deserialize
        const newsByCategory = new Map();

        data.sources.forEach(data => {
          if (newsByCategory.has(data.category)) {
            const newsForCategory = newsByCategory.get(data.category);
            newsForCategory.push({
              id: data.id,
              name: data.name,
              description: data.description,
              url:data.url
            });
            newsByCategory.set(data.category, newsForCategory);
          } else {
            newsByCategory.set(data.category, [{
              id: data.id,
              name: data.name,
              description: data.description,
              url:data.url
            }]);
          }
        });
        // simulate API waiting 4 sec
        setTimeout(() => this.updateState(globals.NEWS_BY_CATEGORY, newsByCategory), 4000);
      }
    });
  }
}

window.instanceMainApp = new MainApp();

window.instanceMainApp.run();
