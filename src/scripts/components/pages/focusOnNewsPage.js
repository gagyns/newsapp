import { globals } from '../../globals';
import { backBtnFocusPageRouteSwitch, create_Image_Element, createHtmlElement } from '../../helperFunstions';

/**
 * class FocusOnNewsPage last page in tree structure in our app. Show selected article
 * @method drawPage draw page content and draw back button witch destination is dinamic because this page is used on
 *    top news part of app, category news part of part and search part.
 */
export class FocusOnNewsPage {
  constructor(classObj) {
    this.maxTitleChar = 150;
    this.mainClassObj = classObj;
    this.$mainContainer = classObj.$mainContentContainer;
    this.labels = classObj.labels;
    this.article = classObj.getStateByEntity(globals.FOCUSED_ARTICLE);
    this.focusOnTopNewsOrCategoryNews = classObj.getStateByEntity(globals.PAGE_ACTIVE_PAGE) === globals.FOCUC_PAGE;
    this.backBtnRoute = backBtnFocusPageRouteSwitch(classObj.getStateByEntity(globals.PAGE_ACTIVE_PAGE));
    this.drawPage();
  }
  drawPage() {
    const backBtn = createHtmlElement('button', 'back-button', '<< Back');
    backBtn.click(() => this.mainClassObj.routeChange(this.backBtnRoute));
    const $title = this.focusOnTopNewsOrCategoryNews ? createHtmlElement('div',
      'news-box-title u_width-90pct u_txt-huge u_txt-align-left mouse-event-none u_margin-top-2rem',
      `${this.article.title.length > this.maxTitleChar ?
        `${this.article.title.substr(0, this.maxTitleChar)} ...` : this.article.title}`)
      : createHtmlElement('div',
          'news-box-title u_width-90pct u_txt-big u_txt-align-center mouse-event-none u_overflow-ellipsis',
          `${this.article.name}`);
    const image = this.article.urlToImage? create_Image_Element(
      'u_width-40pct u_margin-top-small mouse-event-none u_float-left u_margin-right',
      (this.article.urlToImage),
      this.labels.i18n(globals.IMG_LOADING)
    ) : '';
    const description = createHtmlElement('p', 'o_article-test', this.article.description);
    const newContentArticle = createHtmlElement(
      'p',
      ' u_width-50pct o_article-test u_float-left u_margin-top-small ',
      this.article.content ? this.article.content : this.labels.i18n(globals.NO_CONTENT));

    const newContentContainer = createHtmlElement('div', 'o_news-container',
      [image, newContentArticle, createHtmlElement('span', 'u_clearfix'), description]);
    this.$mainContainer.append([backBtn, $title, newContentContainer]);
  }
}