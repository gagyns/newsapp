import { globals } from '../../globals';
import { createHtmlElement, create_Image_Element } from '../../helperFunstions';

/**
 * class CategoriesPage page represent categories with short list of top 5 in category. can be colapsed and expand.
 * @method drawPage draw category boxes and make it collapsed by default
 * @method clickOnCategoryBox expand clicked category box and show top 5 news for category
 * @method focusOnCategory update state with selected category to list and route us to focus on category page
 */
export class CategoriesPage {
  constructor(classObj) {
    this.mainClassObj = classObj;
    this.desktopMode = classObj.getStateByEntity(globals.LAYOUT) === globals.LAYOUT_DESKTOP;
    this.$mainContainer = classObj.$mainContentContainer;
    this.newsByCategories = classObj.getStateByEntity(globals.NEWS_BY_CATEGORY);
    this.labels = classObj.labels;
    this.drawPage();
  }
  drawPage() {
    const categories = [... this.newsByCategories.keys()]
      .map(category => {
        const top5articles = this.newsByCategories.get(category);
        const moreForCategory = createHtmlElement( 'p',
          'u_position-absolute u_txt-small u_mouse_over-text',
          `${this.mainClassObj.labels.i18n(globals.CATEGORY_MORE)}`,
          [{property: 'right', value: '20px'}, {property: 'top', value: '2px'}]);
        const arrowComponent = create_Image_Element(
          'o_arrow u_position-absolute u_postition-top-left-null media_arrow-icon-size a_arrow',
          '../src/resource/arrow-right.svg');
        const categoryName = createHtmlElement('div', 'u_txt-big u_position-relative',
          [
            arrowComponent,
            `<p class="media_text-align">${this.mainClassObj.labels.i18n(`category_${category}`)}</p>`,
            moreForCategory,
          ]);
        const newsPack = top5articles.splice(0, 5)
          .map(news => createHtmlElement('div', 'u_txt-small u_txt-align-left u_margin-left-30px',
            `${news.description}`));
        const categoryBox = createHtmlElement(
          'div',
          `u_overflow-hidden u_margin-top-2rem u_vertical-align-middle u_margin-auto ' +
        ' u_border-radius-10px u_border-gray u_txt-align-center u_txt-huge coursor-pointer u_width-90pct a_category-box`,
          [categoryName, ...newsPack],
          [{property: 'line-height', value: '50px'}]
        );
        categoryBox.click(() => this.clickOnCategoryBox(categoryBox, category, arrowComponent));
        moreForCategory.click(() => this.focusOnCategory(category));

        return categoryBox;
      });
    const categoriesContainer = createHtmlElement(
      'div',
      'o_infobox-container u_margin-auto u_height-100pct u_overflow-scroll',
      [...categories]);
    this.$mainContainer.append(categoriesContainer);
  }
  clickOnCategoryBox(categoryBox, category, arrowComponent) {
    arrowComponent.toggleClass('a_arrow--down');
    categoryBox.toggleClass('a_category-box--expand');
    // const newsForCategory = (this.mainClassObj.getStateByEntity(globals.NEWS_BY_CATEGORY)).get(category);
  }
  focusOnCategory(category) {
    this.mainClassObj.updateStateWithoutRender(globals.FOCUSED_CATEGORY, category);
    this.mainClassObj.updateStateWithoutRender(globals.NEWS_FOR_CATEGORY, this.newsByCategories.get(category));
    this.mainClassObj.routeChange(globals.PAGE_FOCUS_CATEGORY);
  }
}