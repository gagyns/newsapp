import { globals } from '../../globals';
import { createHtmlElement, create_Image_Element, articleSourceAndMoreBtnRoute, moveNews } from '../../helperFunstions';

/**
 * class TopNewsPage main page of app we use this class for also routes categorie list and search list
 * @method constructor initialize class with main class object labels active page in store
 * @method drawNewsBoxies draw main news boxes containers with tree in row.
 * @method createNewsBox create content of new box (title, description, image, more lonk)
 * @method makeTitleForNewBox make title if it is available and for category usage of page make instead of tittle name in box
 *    also in that case make buttons for reordering news in category
 * @method imageOrDescriptionInBox draw image if have link od desription if dont
 * @method focusOnArticle change route to focus on selected news
 */
export class TopNewsPage {
  constructor(classObj) {
    this.mainClassObj = classObj;
    this.labels = classObj.labels;
    this.maxTitleChar = classObj.getStateByEntity(globals.LAYOUT) === globals.LAYOUT_DESKTOP ? 80 : 50;
    this.$mainContainer = classObj.$mainContentContainer;
    this.activePageInStore = classObj.getStateByEntity(globals.PAGE_ACTIVE_PAGE);
    // We use same page for TOP NEWS and ror CATEGORY LIST and SEARCH so we have to switch article data source
    articleSourceAndMoreBtnRoute(classObj, this);
    this.drawNewsBoxies();
  }
  drawNewsBoxies() {
    // When we use top news page as category news list we need back to category button
    const backBtn = createHtmlElement('button', 'back-button', this.labels.i18n(globals.BACK_BUTTON));
    backBtn.click(() => this.mainClassObj.routeChange(globals.PAGE_CATEGORIES));

    // Make news boxies in row of tree
    let arrayOfBox = [];
    const newsBoxes = this.articles.map((data, index) => {
      arrayOfBox.push(this.createNewsBox(data, index));

      if (arrayOfBox.length === 3 || index === this.articles.length -1) {
        const clearFix = createHtmlElement('span', 'u_clearfix');
        const rowOfBox = createHtmlElement('div', 'rowOfBoxElement', [...arrayOfBox, clearFix]);
        arrayOfBox = [];
        return rowOfBox;
      }
    });
    const newsBoxContainer = createHtmlElement(
      'div',
      'o_infobox-container u_margin-auto u_height-100pct u_overflow-scroll',
      [...newsBoxes, createHtmlElement('span', 'u_clearfix')]);
    const backOnPageCondition = this.activePageInStore === globals.PAGE_TOP_NEWS || this.activePageInStore === globals.PAGE_SEARCH ;
    this.$mainContainer.append([
      backOnPageCondition ? '' : backBtn,
      newsBoxContainer
    ]);
  }
  createNewsBox(newsArticle, index) {
    // Use this page also and for bategory listing so we have to make dinamic width for two scenarios of desktop
    const desktopWidthTopOrCategory = this.activePageInStore === globals.PAGE_FOCUS_CATEGORY ? 'media_category-list' : '';
    // In categories list we dont have title provided by API
    const $title = this.makeTitleForNewBox(newsArticle, index);
    // In categories list we dont have img provided by API
    const imageOrDescription = this.imageOrDescriptionInBox(newsArticle);
    // We dont need more for categories list
    const $moreLink = createHtmlElement(
      'div',
      'u_txt-align-right u_float-right coursor-pointer u_position-absolute u_mouse_over-text',
      this.labels.i18n(globals.MORE_LINK),
      [{property: 'right', value: '10px'}, {property: 'bottom', value: '10px'}]
    );

    const $newsBoxElement = createHtmlElement(
      'div',
      `o_newsbox u_float-left u_border-gray u_border-radius-20px u_position-relative
        u_margin-top-2rem u_padding-25px newsBox-width-media ${desktopWidthTopOrCategory} ${this.activePageInStore === globals.PAGE_TOP_NEWS 
        || this.activePageInStore === globals.PAGE_SEARCH ?
        'u_heigh-35vh' : 'u_heigh-10vh'} u_txt-align-center`,
      [$title, imageOrDescription, $moreLink]
    );
    $moreLink.on('click', () => this.focusOnArticle(newsArticle));

    return $newsBoxElement;
  }
  makeTitleForNewBox(newsArticle, index) {
    let title;
    if (newsArticle.title) {
      title = createHtmlElement('div',
        'news-box-title u_width-90pct u_txt-big u_txt-align-left mouse-event-none u_overflow-ellipsis',
        `${newsArticle.title.length > this.maxTitleChar ?
          `${newsArticle.title.substr(0, this.maxTitleChar)} ...` : newsArticle.title}`);
    } else {
      // Cetegory list variant of page
      const upSign = createHtmlElement('div',
        'u_txt-big u_float-right u_margin-left-10px u_mouse_over-text coursor-pointer',
        ['<img src="../src/resource/arrow-alt-up.svg" width="20px" class="mouse-event-none">']);
      upSign.click(() => {
        const newArticlesOrder = moveNews(this.articles, index, 'UP');
        this.mainClassObj.updateStateWithoutRender(globals.NEWS_FOR_CATEGORY, newArticlesOrder);
        this.mainClassObj.routeChange(globals.PAGE_FOCUS_CATEGORY);
      });
      const downSign = createHtmlElement('div',
        'u_txt-big u_float-right u_mouse_over-text coursor-pointer',
        ['<img src="../src/resource/arrow-alt-down.svg" width="20px" class="mouse-event-none">']);
      downSign.click(() => {
        const newArticlesOrder = moveNews(this.articles, index, 'DOWN');
        this.mainClassObj.updateStateWithoutRender(globals.NEWS_FOR_CATEGORY, newArticlesOrder);
        this.mainClassObj.routeChange(globals.PAGE_FOCUS_CATEGORY);
      });

      title = createHtmlElement('div',
        'news-box-title u_width-100pct u_txt-big u_txt-align-center u_overflow-ellipsis',
        [`${newsArticle.name}`, upSign, downSign]);
    }
    return title;
  }
  imageOrDescriptionInBox(newsArticle) {
    const writeDescrtiption = () => {
      const description = createHtmlElement('div',
        'news-box-title u_width-90pct u_txt-small u_txt-align-left mouse-event-none u_overflow-ellipsis',
        `${newsArticle.description ? newsArticle.description : `${this.labels.i18n(globals.NO_DESCRIPTION)}`}`);
      return description;
    };
    let imageOrDescription;
    if (this.activePageInStore === globals.PAGE_TOP_NEWS || this.activePageInStore === globals.PAGE_SEARCH ) {
      imageOrDescription = newsArticle.urlToImage ? create_Image_Element(
        'u_width-90pct u_height-70pct u_margin-top-small mouse-event-none',
        (newsArticle.urlToImage),
        this.labels.i18n(globals.IMG_LOADING)) : writeDescrtiption();
    } else {
      imageOrDescription =  writeDescrtiption();
    }

    return imageOrDescription;
  }
  focusOnArticle(article) {
    this.mainClassObj.updateStateWithoutRender(globals.FOCUSED_ARTICLE, article);
    this.mainClassObj.routeChange(this.routeOnMoreBtn);
  }
}