import * as $ from 'jquery';
import { globals } from '../globals';
import { create_Image_Element, createHtmlElement, isButtonMarkedAsActive } from '../helperFunstions';

/**
 * class HeaderComponent header component witch take care of buttons and search elements in "header" and mobile view side bar
 * @method constructor recieve @props{classObj} main class "this" and assign method in state class to be refreshed on state update
 * @method drawHeader draw basic burger button for mobile view and container for page buttons, search elements and region buttons
 * @method drawPages clean previous pageButtonsContainer and searchContainer fill page buttons container
 * @method drawRegions clean previous newsRegionBtnContainer fill region buttons container
 * @method drawSideBarElements draw side bar HTML element for MOBILE view
 *    TODO make methods for buttons more abstractive to use same methods as drawPage do.
 * @method drawPageButton draw single page button with logic is active and is all data for category loaded
 * @method drawRegionButtons draw single region button with logic is active and are we on focus on news page
 * @method drawFilter draw filter elements
 * @method clearFilter clear filte value and set new data in state
 * @method filterNewsAndPresent filter data from store and apply regexp for identification of news witch have input term in TITLE
 * @method clickOnPageBtn @param page: string on click on button route application on selected page
 * @method clickOnRegionBtn change region update state and run get data for new data set and after receive update news list
 */
export class HeaderComponent {
  constructor(classObj) {
    this.mainClassObj = classObj;
    this.mainClassObj.assignMethodForUpdate(this.drawPages.bind(this));
    this.mainClassObj.assignMethodForUpdate(this.drawRegions.bind(this));
    this.mainClassObj.assignMethodForUpdate(this.drawSideBarElements.bind(this));
  }
  drawHeader() {
    // this.burgerButton = create_Image_Element('u_float-left big-screen-hide coursor-pointer', './resource/ham_ico.svg');
      this.burgerButton = createHtmlElement('div',
      'u_float-left big-screen-hide media_button-padding u_txt-big coursor-pointer', ['<img src="../src/resource/ham_ico.svg" width="20px" class="mouse-event-none">']);
    this.burgerButton.click(() => this.mainClassObj.$sideBarContainer.toggleClass('o_side-bar-menu--inactive'));
    this.pageButtonsContainer = createHtmlElement('div', 'u_float-left small-media-hide');
    this.sideBarPageButtonsContainer = createHtmlElement('div', 'u_float-left');

    this.searchContainer = createHtmlElement('div',
      'u_float-left  small-media-hide u_margin-top-small u_margin-left');
    this.newsRegionBtnContainer = createHtmlElement('div', 'u_float-right');
    this.drawPages();
    this.drawRegions();
    this.drawSideBarElements();
    this.mainClassObj.$sideBarContainer.append(this.sideBarPageButtonsContainer);
    this.mainClassObj.$headerContainer
      .append([this.burgerButton, this.pageButtonsContainer, this.searchContainer, this.newsRegionBtnContainer, createHtmlElement('span', 'u_clearfix')]);
  }
  drawPages() {
    const activePage = this.mainClassObj
      .getStateByEntity(globals.PAGE_ACTIVE_PAGE);
    this.pageButtonsContainer.empty();
    this.searchContainer.empty();
    const pageBtns = this.mainClassObj.getStateByEntity('pagesTypes')
      .map(page => this.drawPageButton(page, activePage));
    // Draw fitler input if we are on searh page
    if (this.mainClassObj.getStateByEntity(globals.PAGE_ACTIVE_PAGE) === globals.PAGE_SEARCH) {
      this.drawFilter();
    }
    // Draw Region buttons
    this.pageButtonsContainer.html([...pageBtns, createHtmlElement('span', 'u_clearfix')]);
  }
  drawRegions() {
    const activeRegion = this.mainClassObj.getStateByEntity(globals.NEWS_REGION);
    this.newsRegionBtnContainer.empty();
    const regionBtn = this.mainClassObj.getStateByEntity('regionAvailable')
      .map(region => this.drawRegionButtons(region, activeRegion));
    // Draw Region Select button
    this.newsRegionBtnContainer.html([...regionBtn, createHtmlElement('span', 'u_clearfix')]);
  }
  drawSideBarElements() {
    this.sideBarPageButtonsContainer.empty();
    const $closeBtn = createHtmlElement('div',
      'u_width-100pct coursor-pointer u_txt-big o_header-button-padding u_txt-align-center u_border-bottom-gray u_mouse_over-text', 'MENI');
    $closeBtn.click(() => this.mainClassObj.$sideBarContainer.toggleClass('o_side-bar-menu--inactive'));
    const pageBtnsContainer = createHtmlElement('div');
    const activePage = this.mainClassObj
      .getStateByEntity(globals.PAGE_ACTIVE_PAGE);
    const pageBtns = this.mainClassObj.getStateByEntity('pagesTypes')
      .map(page => this.drawPageButton(page, activePage, 'sidebar'));
    const filterInputContainer = createHtmlElement('div', 'u_width-100pct u_margin-top-small');
    // TODO create fiter method make more abstract so can use in this case
    if (this.mainClassObj.getStateByEntity(globals.PAGE_ACTIVE_PAGE) === globals.PAGE_SEARCH) {
      const inputField = $('<input>', {
        type: 'text',
        class: 'u_width-100pct u_margin-left',
        placeholder: this.mainClassObj.getStateByEntity(globals.FILTER_REGEX)
      });
      const clearFilterBtn = createHtmlElement('div',
        'u_width-100pct coursor-pointer o_header-button-padding u_txt-align-center u_border-bottom-gray u_mouse_over-text',
        'CLEAR');
      inputField.on('change', () => this.filterNewsAndPresent($(inputField).val()));
      clearFilterBtn.click(() => this.clearFilter());
      filterInputContainer.append([inputField, clearFilterBtn]);
    }

    pageBtnsContainer.append([...pageBtns, filterInputContainer]);
    this.sideBarPageButtonsContainer.append([$closeBtn, pageBtnsContainer]);
  }

  drawPageButton(page, activePage, element) {
    const isNewsByCategoryLoaded = !((this.mainClassObj.getStateByEntity(globals.NEWS_BY_CATEGORY)).size > 0)
      && page === globals.PAGE_CATEGORIES ? 'u_txt-color-lightGray mouse-event-none' : '' ;
    const classSwich = isButtonMarkedAsActive(page, activePage);
    const $button = createHtmlElement(
      'div',
      `${element ? 'u_width-100pct u_txt-align-center u_border-bottom-gray' : 'u_float-left u_border-right-gray'} 
        coursor-pointer u_txt-big o_header-button-padding 
      ${classSwich} ${isNewsByCategoryLoaded}`,
      `<p>${this.mainClassObj.labels.i18n(page)}</p>`);
    $button.click(() => this.clickOnPageBtn(page));

    return $button;
  }
  drawRegionButtons(btn, activeRegion) {
    const activePageInStore = this.mainClassObj.getStateByEntity(globals.PAGE_ACTIVE_PAGE);
    const classSwich = btn === activeRegion ? 'button-animation-gray-fadeIn u_txt-color-white' : '';
    const isOnFocusPage = activePageInStore === globals.FOCUC_PAGE || activePageInStore === globals.FOCUC_PAGE_IN_CATEGORY
      || activePageInStore === globals.FOCUC_PAGE_IN_SEARCH;
    const $button = createHtmlElement('div', `u_float-right coursor-pointer 
      u_txt-big media_button-padding u_border-left-gray ${classSwich} ${isOnFocusPage ?
      'u_txt-color-lightGray mouse-event-none' : ''}`,
      `<p>${this.mainClassObj.labels.i18n(btn)}</p>`);
    $button.click(() => this.clickOnRegionBtn(btn));

    return $button;
  }
  drawFilter() {
    const inputField = $('<input>', {
      type: 'text',
      class: 'u_float-left',
      placeholder: this.mainClassObj.getStateByEntity(globals.FILTER_REGEX)
    });
    const clearFilterBtn = createHtmlElement('div',
      'coursor-pointer u_float-left u_margin-left ',
      ['<img src="../src/resource/close.svg" width="12px" class="mouse-event-none">']);
    inputField.on('change', () => this.filterNewsAndPresent($(inputField).val()));
    clearFilterBtn.click(() => this.clearFilter());
    this.searchContainer.html([inputField, clearFilterBtn]);
  }
  clearFilter() {
    const topNewsData = this.mainClassObj.getStateByEntity(globals.TOP_NEWS_DATA);
    this.mainClassObj.updateStateWithoutRender(globals.FILTER_REGEX, '');
    this.mainClassObj.updateStateWithoutRender(globals.FILTERED_DATA, topNewsData);
    this.mainClassObj.routeChange(globals.PAGE_SEARCH);
  }
  filterNewsAndPresent(filterText) {
    this.mainClassObj.updateStateWithoutRender(globals.FILTER_REGEX, filterText);
    const regex = new RegExp(`${filterText.toLowerCase()}`);
    const topNewsData = this.mainClassObj.getStateByEntity(globals.TOP_NEWS_DATA);
    const filteredData = topNewsData.filter(data => regex.test(data.title.toLowerCase()));
    this.mainClassObj.updateStateWithoutRender(globals.FILTERED_DATA, filteredData);
    this.mainClassObj.routeChange(globals.PAGE_SEARCH);

  }
  clickOnPageBtn(page) {
    this.mainClassObj.routeChange(page);
  }
  clickOnRegionBtn(region) {
    // this.mainClassObj.updateStateWithoutRender(globals.PAGE_ACTIVE_PAGE, globals.PAGE_TOP_NEWS);
    this.mainClassObj.updateState(globals.NEWS_REGION, region);
    this.mainClassObj.getData();
  }
}