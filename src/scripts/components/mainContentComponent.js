import * as $ from 'jquery';
import { createHtmlElement } from '../helperFunstions';
import { globals } from "../globals";

/**
 * class MainContentComponent act as main component class for part of page where we get main content
 * @method clearMainContentContainer clear all content in container
 * @method drawMainContentLoader draw initial loading message
 * @method drawPage draw page witch are received from router class and give that class main class "this" object
 */
export class MainContentComponent {
  constructor(classObj) {
    this.mainClassObj = classObj;
  }
  clearMainContentContainer() {
    this.mainClassObj.$mainContentContainer.empty();
  }
  drawMainContentLoader() {
    this.clearMainContentContainer();
    this.loadinMsg = createHtmlElement('p', 'a_loading',
      this.mainClassObj.labels.i18n(globals.LOADING_MSG));
    this.$loaderContainer = createHtmlElement(
      'div',
      'u_txt-align-center u_align-items-center u_vertical-align-middle', this.loadinMsg,
      [{property: 'line-height', value: '70vh'}]
    );
    this.mainClassObj.$mainContentContainer.append(this.$loaderContainer);
  }
  drawPage(page) {
    this.clearMainContentContainer();
    new page.callClass(this.mainClassObj);
  }
}
