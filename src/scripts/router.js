import { globals } from './globals';
import { TopNewsPage } from './components/pages/topNewsPage';
import { FocusOnNewsPage } from './components/pages/focusOnNewsPage';
import { CategoriesPage } from './components/pages/categoriesPage';
import { State } from './state';

/**
 * @class Router class is initialized by MainApp as extends. Router also extend State and make it accessible in whole application
 * @routeChange relieve route and instantiating component with are corresponding with page call in pageTypes Map.
 */
export class Router extends State {
  constructor() {
    super();
  }
  routeChange(route) {
    this.$sideBarContainer.addClass('o_side-bar-menu--inactive');
    this.updateState(globals.PAGE_ACTIVE_PAGE, route);
    const page = pageTypes.get(route);
    this.mainContentComponent.drawPage(page);
  }
}

/**
 * @pageTypes reference app whitch class to call on witch route
 * @type {Map<*, {callClass}>}
 */
const pageTypes = new Map ([
  [globals.PAGE_TOP_NEWS, {callClass: TopNewsPage}],
  [globals.FOCUC_PAGE, {callClass: FocusOnNewsPage}],
  [globals.PAGE_CATEGORIES, {callClass: CategoriesPage}],
  [globals.PAGE_FOCUS_CATEGORY, {callClass: TopNewsPage}],
  [globals.FOCUC_PAGE_IN_CATEGORY, {callClass: FocusOnNewsPage}],
  [globals.PAGE_SEARCH, {callClass: TopNewsPage}],
  [globals.FOCUC_PAGE_IN_SEARCH, {callClass: FocusOnNewsPage}],
]);