/**
 * @class TopNewsDataModel data model for api response use simple deserialize  pattern
 * @method deserialize deserialize and return object
 * @method getCleanSourceName make small corection in raw data and put source name without spaces an in lowercase
 */
export class TopNewsDataModel {
  constructor () {
    this.author;
    this.content;
    this.description;
    this.publishedAt;
    this.source;
    this.title;
    this.url;
    this.urlToImage;
  }
  deserialize(dataPack, index) {
    const deserializedObj = {...this, ...dataPack, ...{generated_id: `${this.getCleanSourceName(dataPack.source.name)}_${index}`}};
    return deserializedObj;
  }
  getCleanSourceName(sourceName) {
    return sourceName.replace(/\s/g, '_').toLowerCase();
  }
}