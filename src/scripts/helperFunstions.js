// Pure function builder pattern
import * as $ from 'jquery';
import { globals } from './globals';

/**
 * @createHtmlElement create dynamic HTML element with attributes we give ass inputs
 * @param typeOfLement type of HTML element we want to create
 * @param classNames class name for created HTML element
 * @param htmlContent HTML content in case that ours element have to have nested HTML element
 * @param additionalStyle additional style if we cant do it with class
 * @returns {*|jQuery|HTMLElement}
 */
export const createHtmlElement = (typeOfLement, classNames, htmlContent, additionalStyle) => {
  const element = $(`<${typeOfLement}>`, {
    class: classNames,
  });
  if (htmlContent) {
    element.html(htmlContent);
  }
  if (additionalStyle) {
    additionalStyle.forEach(style => element.css(style.property, style.value));
  }

  return element;
};

/**
 * @create_Image_Element create image HTML element
 * @param classNames class name for created HTML element
 * @param src path to the image
 * @param alt message while we wait image to load
 * @returns {*|jQuery|HTMLElement}
 */
export const create_Image_Element = (classNames, src, alt) => {
  const element = $('<img>', {
    class: classNames,
    src: src,
    alt: alt,
  });
  return element;
};

/**
 * @isButtonMarkedAsActive determine witch main button is active on witch page
 * @param btnType button witch activity status we want to determine
 * @param activePageInStore page in store witch is active in that moment in app
 * @returns {string}
 */
export const isButtonMarkedAsActive = (btnType, activePageInStore) => {
  const activeBtnClass = 'button-animation-gray-fadeIn u_txt-color-white';
  const isButtonActiveOnPage = btnType === activePageInStore;
  const focusActivePageBtn = btnType === globals.PAGE_TOP_NEWS && activePageInStore === globals.FOCUC_PAGE;
  const categoryBtnActive = (btnType === globals.PAGE_CATEGORIES && activePageInStore === globals.PAGE_FOCUS_CATEGORY)
  || (btnType === globals.PAGE_CATEGORIES && activePageInStore === globals.FOCUC_PAGE_IN_CATEGORY);
  const searchBtnActive = btnType === globals.PAGE_SEARCH && activePageInStore === globals.FOCUC_PAGE_IN_SEARCH;

  const conditionComposer = isButtonActiveOnPage || focusActivePageBtn || categoryBtnActive || searchBtnActive;

  return conditionComposer ? activeBtnClass : '';
};

/**
 * articleSourceAndMoreBtnRoute "MORE" button have to have dynamical settings because we used in in different part of application
 * @param classObj main class "this" object to get access to the store @getStateByEntity method
 * @param componentThis instanced class "this" to get access to the properties which we use to set Back and More behavior
 */
export const articleSourceAndMoreBtnRoute = (classObj, componentThis) => {
  switch(classObj.getStateByEntity(globals.PAGE_ACTIVE_PAGE)) {
    case globals.PAGE_TOP_NEWS:
      componentThis.articles = classObj.getStateByEntity(globals.TOP_NEWS_DATA);
      componentThis.routeOnMoreBtn = globals.FOCUC_PAGE;
    break;
    case globals.PAGE_FOCUS_CATEGORY:
      componentThis.articles = classObj.getStateByEntity(globals.NEWS_FOR_CATEGORY);
      componentThis.routeOnMoreBtn = globals.FOCUC_PAGE_IN_CATEGORY;
    break;
    case globals.PAGE_SEARCH:
      componentThis.articles = classObj.getStateByEntity(globals.FILTERED_DATA);
      componentThis.routeOnMoreBtn = globals.FOCUC_PAGE_IN_SEARCH;
      break;
    default:
      componentThis.articles = classObj.getStateByEntity(globals.TOP_NEWS_DATA);
    break;
  }
};

/**
 * @backBtnFocusPageRouteSwitch help us to have dynamical "BACK" button behavior
 * @param activePage
 * @returns {string}
 */
export const backBtnFocusPageRouteSwitch = (activePage) => {
  switch (activePage) {
    case globals.FOCUC_PAGE:
      return globals.PAGE_TOP_NEWS;
    break;
    case globals.FOCUC_PAGE_IN_CATEGORY:
      return globals.PAGE_FOCUS_CATEGORY;
    break;
    case globals.FOCUC_PAGE_IN_SEARCH:
      return globals.PAGE_SEARCH;
    break;
    default:
      return globals.PAGE_TOP_NEWS;
    break;
  }
};

/**
 * @moveNews method rearrange news list for category
 * @param newsArray array in witch are we want to do manipulation
 * @param originalIndex index of chosen object witch we want to "move"
 * @param direction direction witch we want to our object to be moved in array
 * @returns {new composed array}
 */
export const moveNews = (newsArray, originalIndex, direction) => {
  const originalElement = newsArray[originalIndex];
  const nextElement = newsArray[direction === 'UP' ? originalIndex - 1 : originalIndex + 1];
  const firstPart = newsArray.slice(0, (direction === 'DOWN' ? originalIndex : originalIndex -1));
  const restOfArray = newsArray.slice((direction === 'DOWN' ? originalIndex + 1 : originalIndex), newsArray.length);
  const orderOfAffectedElements = direction === 'DOWN' ? [nextElement, originalElement] : [originalElement, nextElement];
  const composeNewArray = [...firstPart, ...orderOfAffectedElements, ...restOfArray];
  return (direction === 'UP' && originalIndex === 0)
  || (direction === 'DOWN' && originalIndex >= newsArray.length -1) ? newsArray : composeNewArray;
};