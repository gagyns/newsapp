import { englishObj } from './english';
import { serbianObj } from './serbian';

/**
 * @class I18n work as i18n script and receive selected language from app state and set main language object
 * @method i18n @param key recieve value for label and return selected language object value if key not exist return key as value
 */
export class I18n {
  constructor(selectedLanguage) {
    this.selectedLanguage = selectedLanguage;
    this.languageObject = { english: englishObj, serbian: serbianObj };
  }
  i18n(key) {
    const label = this.languageObject[this.selectedLanguage][key];

    return label ? label : key;
  }
}